require "rails_helper"

RSpec.feature "Creating Product" do
	before {
		cat1 = Category.create({name: 'Men'})
		cat2 = Category.create({name: 'Women'})
	}

	scenario "A user creates a new product" do

		visit "/products"

		click_link "New Product"

		fill_in "Name", with: "Test Product"
		fill_in "Description", with: "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
		fill_in "Price", with: "10.00"
		select("Men", :from => "product_category_id")

		click_button "Create Product"

		expect(page).to have_content("Product was successfully created")
		expect(page.current_path).to eq(products_path)
	end

	scenario "A user fails to create a new product" do
		visit "/products"

		click_link "New Product"

		fill_in "Name", with: ""
		fill_in "Description", with: ""
		fill_in "Price", with: ""
		select "Choose a category", :from => "product_category_id"

		click_button "Create Product"

		expect(page).to have_content("Name can't be blank")
		expect(page).to have_content("Description can't be blank")
		expect(page).to have_content("Price can't be blank")
	end

end