require 'rails_helper'

RSpec.describe PageController, type: :controller do

	# tests that landing page loads successfully
	describe "GET #index" do
	  it "responds succesfully with an HTTP 200 status code" do
	  	get :index
	  	expect(response).to be_success
	  	expect(response).to have_http_status(200)
	  end

	  it "renders the index template" do
	  	get :index
	  	expect(response).to render_template("index")
	  end
	end

	# tests that about page loads successfully
	describe "GET #about" do
	  it "responds succesfully with an HTTP 200 status code" do
	  	get :about
	  	expect(response).to be_success
	  	expect(response).to have_http_status(200)
	  end

	  it "renders the about template" do
	  	get :about
	  	expect(response).to render_template("about")
	  end
	end

	# tests that contact page loads successfully
	describe "GET #contact" do
	  it "responds succesfully with an HTTP 200 status code" do
	  	get :contact
	  	expect(response).to be_success
	  	expect(response).to have_http_status(200)
	  end

	  it "renders the contact template" do
	  	get :contact
	  	expect(response).to render_template("contact")
	  end
	end

end
