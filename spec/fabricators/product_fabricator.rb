Fabricator(:product) do
  name { Faker::Commerce.product_name }
  description { Faker::Lorem.characters(250) }
  price { Faker::Commerce.price }
  is_active { "1" }
end