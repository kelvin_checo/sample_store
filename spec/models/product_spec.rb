require "rails_helper"

RSpec.describe Product, :type => :model do 

  # test the presence of the required attributes
  it "requires a name" do
  	product = Fabricate.build(:product, name: nil)

  	expect(product).not_to be_valid
  	expect(product.errors[:name].any?).to be_truthy
  end

  it "requires a description" do
  	product = Fabricate.build(:product, description: nil)

  	expect(product).not_to be_valid
  	expect(product.errors[:description].any?).to be_truthy
  end

  it "requires a price" do
  	product = Fabricate.build(:product, price: nil)

  	expect(product).not_to be_valid
  	expect(product.errors[:price].any?).to be_truthy
  end

  #it "requires is_active" do
  #	product = Fabricate.build(:product, is_active: nil)

  #	expect(product).not_to be_valid
  #	expect(product.errors[:is_active].any?).to be_truthy
  #end

  # test numericality of price
  it "requires price to be a number" do
  	product = Fabricate.build(:product, price: "ABC")

  	expect(product).not_to be_valid
  	expect(product.errors[:price].any?).to be_truthy
  end

  # test the value of price is at least a cent
  it "requires price to be greater than or equal to 0.01" do
  	product = Fabricate.build(:product, price: -1)

  	expect(product).not_to be_valid
  	expect(product.errors[:price].any?).to be_truthy
  end

  # test maximum character length
  it "requires name has a maximum length of 50" do
  	product = Fabricate.build(:product, name: Faker::Lorem.characters(100))

  	expect(product).not_to be_valid
  	expect(product.errors[:name].any?).to be_truthy
  end

  it "requires description has a maximum length of 250" do
  	product = Fabricate.build(:product, description: Faker::Lorem.characters(300))

  	expect(product).not_to be_valid
  	expect(product.errors[:description].any?).to be_truthy
  end

end 