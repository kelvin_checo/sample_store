class Category < ActiveRecord::Base
	# define table
	self.table_name = "products_category"

	# define relationships
	has_many :subcategories, :foreign_key => "parent_id", :dependent => :destroy
	has_many :products

	#validation rules
	validates_presence_of	:name

	def self.list
		where(:parent_id => nil)
	end
	
end
