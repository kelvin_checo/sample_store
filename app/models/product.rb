class Product < ActiveRecord::Base

	#define relationships
	belongs_to :category
	belongs_to :subcategory

	# validates attribute values are not empty
	validates_presence_of	:name, :description, :price
	#validates attribute values maximum length
	validates_length_of 	:name, maximum: 50
	validates_length_of 	:description, maximum: 250
	#validates attribute is a currency
	validates 				:price, numericality: {greater_than_or_equal_to: 0.01}
	#validates attribute is a boolean and default is true
	#validates 				:is_active, inclusion: { in: [true, false]}

end
