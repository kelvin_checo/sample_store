class Subcategory < Category

	belongs_to :category, :foreign_key => "parent_id"

	def self.list(catid)
		where("parent_id = ?", catid) if catid.present?
	end

end
