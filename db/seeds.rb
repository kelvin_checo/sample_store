# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).

# Create seed for products table (Fabricator is located in '/spec/fabricators/product_fabricator.rb')
Product.destroy_all

10.times { Fabricate(:product) }