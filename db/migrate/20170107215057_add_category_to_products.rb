class AddCategoryToProducts < ActiveRecord::Migration
  def self.up
  	change_table :products do |t|
  		t.references :products_category
  	end
  end

  def self.down
  	remove_reference(:products, :products_category)
  end
end
