class RemoveUpdateCategoriesTable < ActiveRecord::Migration
  def change
  	change_table :products_category do |t|
  		t.remove :parent_id_id
  	end
  end
end
