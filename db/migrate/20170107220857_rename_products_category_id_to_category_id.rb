class RenameProductsCategoryIdToCategoryId < ActiveRecord::Migration
  def change
  	rename_column(:products, :products_category_id, :category_id)
  end
end
