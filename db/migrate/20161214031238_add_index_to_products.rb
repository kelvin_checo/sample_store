class AddIndexToProducts < ActiveRecord::Migration
  def change
  	change_table :products do |t|
  		t.index :description
  	end
  end
end
