class CreateProducts < ActiveRecord::Migration
  
  def self.up
    create_table :products do |t|

    	t.string 	:name, limit: "50", null: false, index: true
    	t.text 		:description, limit: "250"
    	t.float 	:price, null: false
    	t.float 	:weight
    	t.boolean :is_active, default: "1", null: false

      t.timestamps null: false
    end
  end

  def self.down
  	drop_table :products
  end
end
