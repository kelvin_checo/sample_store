class CreateProductsCategoryTable < ActiveRecord::Migration
  def self.up
    create_table :products_category do |t|
    	t.integer :parent_id
    	t.string	:name, limit: "50", null: false, index: true

    	t.timestamps
    end
  end

  def self.down
  	drop_table :products_category
  end
end
