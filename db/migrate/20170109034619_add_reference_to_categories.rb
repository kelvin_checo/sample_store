class AddReferenceToCategories < ActiveRecord::Migration
  def change
  	change_table :products_category do |t|
  		t.references :parent_id, foreign_key: true
  	end
  end
end
